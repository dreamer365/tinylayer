
/* eslint-env node */
require( "@rushstack/eslint-patch/modern-module-resolution" );

module.exports = {
	root: true,
	env: {
		browser: true,
		es2022: true,
		node: true
	},
	extends: [
		"plugin:vue/vue3-essential",
		"eslint:recommended"
	],
	parserOptions: {
		ecmaVersion: "latest"
	},
	rules: {
		"vue/no-v-model-argument": "off",
		"array-bracket-spacing": [ 2, "always" ],
		"arrow-body-style": [ 1, "as-needed" ],
		"arrow-parens": [ 1, "as-needed" ],
		"arrow-spacing": [
			1,
			{
				before: true,
				after: true
			}
		],
		"block-spacing": [ 2, "always" ],
		"brace-style": [ 2, "1tbs" ],
		"computed-property-spacing": [ 2, "always" ],
		"comma-dangle": [ 2, "never" ],
		"comma-spacing": [
			2,
			{
				before: false,
				after: true
			}
		],
		"curly": [ 2, "all" ],
		"eqeqeq": [ 2, "always" ],
		"func-call-spacing": [ 2, "never" ],
		"key-spacing": [
			2,
			{
				beforeColon: false,
				afterColon: true
			}
		],
		"lines-around-comment": [
			2,
			{
				beforeLineComment: true
			}
		],
		"max-statements-per-line": [
			"error",
			{
				max: 3
			}
		],

		// "no-alert": 2,
		"no-await-in-loop": 2,
		"no-caller": 2,
		"no-class-assign": 2,
		"no-const-assign": 2,
		"no-delete-var": 2,
		"no-dupe-keys": 2,
		"no-dupe-args": 2,
		"no-duplicate-case": 2,
		"no-empty-character-class": 2,
		"no-eval": 2,
		"no-extend-native": 2,
		"no-extra-bind": 2,
		"no-extra-label": 2,
		"no-extra-semi": 2,
		"no-floating-decimal": 2,
		"no-labels": 2,
		"no-lone-blocks": 2,
		"no-implicit-coercion": 0,
		"no-implied-eval": 2,
		"no-irregular-whitespace": 0,
		"no-multi-str": 2,
		"no-native-reassign": 2,
		"no-proto": 2,
		"no-return-assign": 2,
		"no-confusing-arrow": [
			2,
			{
				allowParens: true
			}
		],
		"no-duplicate-imports": [
			2,
			{
				includeExports: true
			}
		],
		"no-undef-init": 2,
		"no-undefined": 0,
		"no-useless-computed-key": 2,
		"no-useless-rename": 2,
		"no-multi-spaces": 2,
		"no-multiple-empty-lines": [
			2,
			{
				max: 1
			}
		],
		"no-eq-null": 2,
		"no-var": 2,
		"no-inline-comments": 2,
		"no-mixed-spaces-and-tabs": 2,
		"no-multi-assign": 2,
		"no-new": 2,
		"no-new-object": 2,
		"no-new-require": 2,
		"no-undef": 2,
		"no-unused-vars": 0,
		"prefer-rest-params": 2,
		"prefer-template": 1,
		"quotes": [ 2, "double" ],
		"rest-spread-spacing": [ 1, "never" ],
		"semi-spacing": [
			2,
			{
				before: false,
				after: true
			}
		],
		"semi-style": [ 2, "last" ],
		"space-before-function-paren": [ 2, "always" ],
		"space-before-blocks": [ 2, "always" ],
		"space-in-parens": [ 2, "always" ],
		"spaced-comment": [ 2, "always" ],
		"space-infix-ops": 2,
		"switch-colon-spacing": [
			2,
			{
				after: true
			}
		],
		"template-curly-spacing": [ 1, "always" ],
		"vue/multi-word-component-names": [ 0 ],
		"vue/no-reserved-component-names": [ 0 ],
		"vue/attribute-hyphenation": [ 2 ],
		"vue/first-attribute-linebreak": [
			2,
			{
				singleline: "ignore",
				multiline: "below"
			}
		],
		"vue/html-closing-bracket-newline": [
			2,
			{
				singleline: "never",
				multiline: "always"
			}
		],
		"vue/html-closing-bracket-spacing": [
			2,
			{
				startTag: "never",
				endTag: "never",
				selfClosingTag: "always"
			}
		],
		"vue/html-end-tags": [ 2 ],
		"vue/html-indent": [
			2,
			"tab",
			{
				attribute: 1,
				baseIndent: 1,
				closeBracket: 0,
				alignAttributesVertically: false
			}
		],
		"vue/html-quotes": [ 2, "double" ],
		"vue/max-attributes-per-line": [
			2,
			{
				singleline: {
					max: 4
				},
				multiline: {
					max: 1
				}
			}
		],
		"vue/mustache-interpolation-spacing": [ 2, "always" ],
		"vue/no-multi-spaces": [ 2 ],
		"vue/no-spaces-around-equal-signs-in-attribute": [ 2 ],
		"vue/no-template-shadow": [ 2 ],
		"vue/one-component-per-file": [ 2 ],
		"vue/prop-name-casing": [ 2, "camelCase" ],
		"vue/v-bind-style": [ 2, "shorthand" ],
		"vue/v-on-style": [ 2, "shorthand" ],
		"vue/component-tags-order": [
			2,
			{
				order: [ "template", "script", "style" ]
			}
		],
		"vue/script-indent": [
			2,
			"tab",
			{
				baseIndent: 1,
				switchCase: 1
			}
		],
		"vue/this-in-template": [ 2, "never" ],
		"vue/attributes-order": [
			2,
			{
				order: [
					"DEFINITION",
					"LIST_RENDERING",
					"CONDITIONALS",
					"RENDER_MODIFIERS",
					"GLOBAL", [ "UNIQUE", "SLOT" ],
					"TWO_WAY_BINDING",
					"OTHER_DIRECTIVES",
					"OTHER_ATTR",
					"EVENTS",
					"CONTENT"
				],
				alphabetical: false
			}
		],
		"linebreak-style": [ "off", "windows" ]
	},
	globals: {
		window: true,
		document: true,
		$tinylayer: true,
		defineProps: "readonly",
		defineEmits: "readonly",
		defineExpose: "readonly",
		defineOptions: "readonly"
	}
};