
<br>
<h1 align="center">TinyLayer</h1>
<p align="center">使用 Vue 开发的桌面端弹窗组件</p>
<p align="center">
  <img src="https://img.shields.io/badge/npm-v2.2.2-blue.svg">
  <img src="https://img.shields.io/badge/license-MIT-green.svg">
  <img src="https://img.shields.io/badge/vue-3.2+-orange.svg">
</p>
<p align="center">
  <a href="https://www.zgcoder.com/tinylayer/" target="_blank">组件文档</a>
</p>

<hr/>

## 模式

- Loading 模式
- Alert 模式
- Confirm 模式
- Prompt 模式
- Modal 模式

<hr/>

## 安装
```bash
npm i tinylayer -S
```

<hr/>

## 引入

```js
// main.js 中引入并注册 
import { createApp } from "vue";
import App from "./App.vue";
const app = createApp( App );

import TinyLayer from "tinylayer";
app.use( TinyLayer );
```

<hr/>

## 浏览器支持
<table>
    <tr>
        <td>Chrome</td>
        <td>Firefox</td>
        <td>Edge</td>
        <td>Safari</td>
        <td>Opera</td>
    </tr>
    <tr>
        <td>last 2 versions</td>
        <td>last 2 versions</td>
        <td>last 2 versions</td>
        <td>last 2 versions</td>
        <td>last 2 versions</td>
    </tr>
</table>