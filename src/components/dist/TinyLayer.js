import { createApp } from "vue";
import TinyLayer from "./TinyLayer.vue";
import util from "./util/index.js";

if ( typeof window !== "undefined" ) {
	if ( !document.querySelector( ".__TINYLAYER-ROOT-CONTAINER__" ) ) {
		const section = document.createElement( "section" );
		section.className = "__TINYLAYER-ROOT-CONTAINER__";
		document.body.appendChild( section );
	}

	const createInstance = () => {
		const create = () => {
			const root = document.querySelector( ".__TINYLAYER-ROOT-CONTAINER__" );
			Array.from( root.childNodes ).forEach( node => {
				if ( node.nodeType === 8 ) {
					root.removeChild( node );
				}
			} )
			createApp( TinyLayer ).mount( document.createElement( "div" ) );
		}
		const url = window.location.href;
		if ( !url.includes( "#" ) ) {
			create();
		} else {
			if ( url.slice( url.indexOf( "#" ) + 1 ).startsWith( "/" ) ) {
				create();
			}
		}
	}

	const rewritePushReplace = function ( type ) {
		const originalHistory = window.history[ type ];
		return function ( ...arg ) {
			const transfer = originalHistory.apply( this, arg );
			const ev = new Event( type );
			ev.arguments = arg;
			window.dispatchEvent( ev );
			return transfer;
		}
	}
	window.history.pushState = rewritePushReplace( "pushState" );
	window.history.replaceState = rewritePushReplace( "replaceState" );

	[ "hashchange", "popstate", "pushState", "replaceState" ].forEach( item => {
		window.addEventListener( item, () => {
			createInstance();
		} )
	} )

	TinyLayer.install = ( Vue, options ) => {
		if ( util.isPlainObject( options ) ) {
			window[ Symbol.for( "$tinylayer" ) ] = options;
		}
		Vue.component( "TinyLayer", TinyLayer );
		createInstance();
	}
}

export default TinyLayer;