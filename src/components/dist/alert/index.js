export default {
    type: "",
    title: "",
    content: "",
    zIndex: 2000,
    width: "400px",
    maskBackground: "",
    useHtmlContent: false,
    animationType: "scale",
    afterOpen: () => {},
    afterClose: () => {},
    ok: {
        text: "确定",
        callback: () => {}
    }
}