export default {
    title: "",
    content: "",
    zIndex: 2000,
    width: "400px",
    maskBackground: "",
    useHtmlContent: false,
    animationType: "scale",
    afterOpen: () => {},
    afterClose: () => {},
    ok: {
        text: "确定",
        closable: true,
        disabled: false,
        loading: false,
        callback: () => {}
    },
    cancel: {
        text: "取消",
        disabled: false,
        callback: () => {}
    }
}