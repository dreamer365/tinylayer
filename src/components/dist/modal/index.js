const modalProps = {
    modelValue: {
        type: Boolean,
        default: false
    },
    id: {
        type: String,
        default: ""
    },
    className: {
        type: String,
        default: ""
    },
    zIndex: {
        type: Number,
        default: 1000
    },
    maskVisible: {
        type: Boolean,
        default: true
    },
    maskClosable: {
        type: Boolean,
        default: false
    },
    maskBackground: {
        type: String,
        default: ""
    },
    fullscreen: {
        type: Boolean,
        default: false
    },
    insertIframe: {
        type: String,
        default: ""
    },
    animationType: {
        type: String,
        default: "scale"
    },
    background: {
        type: String
    },
    autoClose: {
        type: Number,
        default: 0
    },
    autoCloseText: {
        type: String,
        default: "秒后关闭"
    },
    autoCloseIntercept: {
        type: Boolean,
        default: false
    },
    autoCloseVisible: {
        type: Boolean,
        default: true
    },
    escClosable: {
        type: Boolean,
        default: true
    },
    draggable: {
        type: Boolean,
        default: false
    },
    bodyScrollable: {
        type: Boolean,
        default: true
    },
    headerVisible: {
        type: Boolean,
        default: true
    },
    footerVisible: {
        type: Boolean,
        default: true
    },
    contentBackground: {
        type: String,
        default: ""
    },
    contentLoading: {
        type: Boolean,
        default: false
    },
    contentLoadingText: {
        type: String,
        default: ""
    },
    contentLoadingBackground: {
        type: String,
        default: ""
    },
    contentHeight: {
        type: String,
        default: "auto"
    },
    noline: {
        type: Boolean,
        default: false
    },
    title: {
        type: String,
        default: ""
    },
    width: {
        type: String,
        default: "450px"
    },
    closeIconVisible: {
        type: Boolean,
        default: true
    },
    fullscreenIconVisible: {
        type: Boolean,
        default: false
    },
    okText: {
        type: String,
        default: "确定"
    },
    okVisible: {
        type: Boolean,
        default: true
    },
    okDisabled: {
        type: Boolean,
        default: false  
    },
    okClosable: {
        type: Boolean,
        default: true
    },
    okLoading: {
        type: Boolean,
        default: false
    },
    cancelText: {
        type: String,
        default: "取消"
    },
    cancelVisible: {
        type: Boolean,
        default: true
    },
    cancelDisabled: {
        type: Boolean,
        default: false
    },
    cancelClosable: {
        type: Boolean,
        default: true
    }
};

const modalEmits = [
    "update:modelValue",
    "fullscreen-change",
    "cancel",
    "ok",
    "when-open",
    "after-open",
    "auto-close-countdown",
    "auto-close-intercept",
    "dragging",
    "drag-end",
    "when-leave",
    "after-close"
];

export { modalProps, modalEmits };