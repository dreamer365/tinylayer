export default {
    title: "",
    defaultValue: "",
    placeholder: "",
    inputType: "text",
    inputDisabled: false,
    pattern: null,
    errorMsg: "",
    isError: false,
    zIndex: 2000,
    width: "370px",
    maskBackground: "",
    tip: "",
    animationType: "scale",
    afterOpen: () => {},
    afterClose: () => {},
    ok: {
        text: "确定",
        closable: true,
        disabled: false,
        loading: false,
        callback: () => {}
    },
    cancel: {
        text: "取消",
        disabled: false,
        callback: () => {}
    }
}