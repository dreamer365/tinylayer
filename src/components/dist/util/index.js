const util = {
    randomString: () => Math.random().toString( 36 ).slice( 2, 10 ),
    getCSS: ( elem, prop ) => {
        if ( elem instanceof HTMLElement && typeof prop === "string" ) {
            return window.getComputedStyle( elem, null ).getPropertyValue( prop );
        }
        return "";
    },
    getType: obj => Object.prototype.toString.call( obj ).slice( 8, -1 ).toLowerCase(),
    isFunction: obj => typeof obj === "function",
    isString: obj => typeof obj === "string",
    isNumber: obj => Number.isSafeInteger( obj ),
    isPlainObject: obj => util.getType( obj ) === "object",
    isRegExp: obj => util.getType( obj ) === "regexp",
    delay: ( func, time = 0 ) => {
        if ( util.isFunction( func ) ) {
            let timer = window.setTimeout( () => {
                window.clearTimeout( timer );
                timer = null;
                func();
            }, Number.isSafeInteger( time ) && time > 0 ? time : 0 )
        }
    },
    clone: data => {
        let newData = null;
        if ( !/array|object/.test( util.getType( data ) ) ) {
            newData = data;
        } else if ( Array.isArray( data ) ) {
            newData = [];
            data.forEach( item => {
                newData.push( util.clone( item ) )
            } )
        } else if ( util.isPlainObject( data ) ) {
            newData = {};
            for ( const i in data ) {
                if ( Array.isArray( data[ i ] ) || util.isPlainObject( data ) ) {
                    newData[ i ] = util.clone( data[ i ] );
                } else {
                    newData[ i ] = data[ i ];
                }
            }
        } else {
            newData = data;
        }
        return newData;
    },
    merge: ( defaults, params ) => {
        const result = {};
        for ( const key in defaults ) {
            const v = params[ key ];
            if ( util.isPlainObject( v ) ) {
                result[ key ] = util.merge( defaults[ key ], v );
            } else {
                result[ key ] = ( v === 0 || v === false ) ? v : ( v || defaults[ key ] );
            }
        }
        return result;
    },
    debounce: ( func, time ) => {
        if ( util.isFunction( func ) ) {
            let timer = null;
            return function () {
                if ( timer ) {
                    window.clearTimeout( timer );
                }
                timer = window.setTimeout( func, util.isNumber( time ) && time > 0 ? time : 100 );
            }
        }
    },
    astrictWidth: data => {
        data.__originalWidth__ = data.width;
        const func = () => {
            const minWidth = 300;
            const windowWidth = document.documentElement.clientWidth;
            const dom = document.createElement( "div" );
            dom.style.cssText = `position:absolute;width:${ data.__originalWidth__ };height:1px;opacity:0;pointer-event:none;`;
            document.body.appendChild( dom );
            const currentWidth = parseInt( util.getCSS( dom, "width" ) );
            if ( currentWidth >= windowWidth ) {
                const targetWidth = windowWidth - 40;
                data.width = `${ targetWidth > minWidth ? targetWidth : minWidth }px`;
            } else if ( currentWidth < minWidth ) {
                data.width = `${ minWidth }px`;
            } else {
            	data.width = data.__originalWidth__;
            }
            document.body.removeChild( dom );
        }
        func();
        window.addEventListener( "resize", util.debounce( () => {
            func();
        }, 50 ) )
    }
};

export default util;