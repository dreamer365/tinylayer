
import { createApp } from "vue";
import App from "./App.vue";
const app = createApp( App );

import TinyLayer from "./components/dist/TinyLayer.js";
app.use( TinyLayer );

app.mount( "#app" );
