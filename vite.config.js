import { fileURLToPath, URL } from "node:url";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

export default () => defineConfig( {
	plugins: [
		vue()
	],
	resolve: {
		alias: {
			"@": fileURLToPath( new URL( "./src", import.meta.url ) )
		}
	},
	base: "/",
	clearScreen: true,
	server: {
		host: "0.0.0.0",
		port: 1133,
		https: false,
		strictPort: false,
		open: false,
		cors: true
	}
} )